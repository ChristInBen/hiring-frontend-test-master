import {createElement} from 'react';
import styles from './styles.css';

const Heading = ({...heading}) => (
  <div className={styles.headerContainer}>
    <span className={styles.cartIcon}>{heading.children}</span>
    <span className={styles.headingContainer}>
      <h1 className={styles.heading}>{heading.title}</h1>
    </span>
  </div>
);

export default Heading;
