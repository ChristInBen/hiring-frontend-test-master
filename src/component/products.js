import {createElement} from 'react';
import Product from './product';
import {FaCoffee} from 'react-icons/lib/fa';
import * as products from '../data/items';
import Heading from './heading';

export default () => (
  <div>
    <Heading title={"Products"}><FaCoffee /></Heading>
    <div>
      <Product {...products.cake}/>
      <Product {...products.waffle}/>
      <Product {...products.chocolate}/>
    </div>
  </div>
);
