import {createElement} from 'react';
import {add} from '../action/cart';
import {connect} from 'react-redux';
import styles from './styles.css';

// Used spread on product to keep the params cleaner.
const Product = ({...product, add, items}) => (
  <div
    className={items.filter((item) => item.id === product.id)[0]
    ? styles.productInactive : styles.productActive}
  >
    <div className={styles.product} onClick={() => add(product.id)}>
      <img
        src={product.image}
        alt={product.title}
        className={styles.productImage}
      />
      <span>{product.title}</span>
    </div>
  </div>
);

export default connect((state) => {
  return {
    items: state.cart.items,
  };
}, {add})(Product);
