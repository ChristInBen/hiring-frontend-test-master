import {createElement} from 'react';
import map from 'lodash/fp/map';
import reduce from 'lodash/fp/reduce';
import {connect} from 'react-redux';

import {clear, setQuantity, remove} from '../action/cart';
import * as products from '../data/items';
import Heading from './heading';
import {FaPlus, FaMinus, FaTrash, FaShoppingCart} from 'react-icons/lib/fa';
import styles from './styles.css';

const Item = connect(
  () => ({}),
  {setQuantity, remove}
)(({id, quantity, remove, setQuantity}) => {
  const {title, price} = products[id];
  const actions = {
    del: () => remove(id),
    inc: () => setQuantity({id, quantity: quantity + 1}),
    dec: () => ((quantity > 1) ? setQuantity({id, quantity: quantity - 1})
      : remove(id)),
  };
  return (
    <tr className={styles.itemAppear}>
      <td>
        {title}
        <a className={styles.removeProduct} onClick={actions.del}>
          <FaTrash />
        </a>
      </td>
      <td>
        {price}
      </td>
      <td>
        <span>
          <span>{quantity}</span>
          <span className={styles.quantityButtonsContainer}>
            <a className={styles.quantityAddButton} onClick={actions.inc}>
              <FaPlus />
            </a>
          </span>
          <span className={styles.quantityButtonsContainer}>
            <a className={styles.quantitySubtractButton} onClick={actions.dec}>
              <FaMinus />
            </a>
          </span>
        </span>
      </td>
      <td>
        ${(price * quantity).toFixed(2)}
      </td>
    </tr>
  );
});

const ActiveCart = ({...props}) => (
  <div>
    <span className={styles.clearCartContainer}>
      <button
        className={styles.clearCart}
        onClick={props.clear}
      > <span>Clear all items</span>
      </button>
    </span>
    <div className={styles.productTableContainer}>
      <table className={styles.productTable}>
        <thead>
          <tr>
            <th>Product</th>
            <th>Price</th>
            <th>Quantity</th>
            <th>Total</th>
            <th />
          </tr>
        </thead>
        <tbody>
          {map((item) => <Item key={item.id} {...item}/>, props.items)}
          <tr>
            <td colSpan={4} />
            <td>
              <span className={styles.totalLabel}>${props.total}</span>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
  </div>
);

const EmptyCart = ({message}) => (
  <div className={styles.emptyCart}>{message}</div>
);

const Cart = connect(
  () => ({}),
  {clear}
)(({total, items, clear}) => {
  return (
    <div>
      <Heading title={"Cart"}><FaShoppingCart /></Heading>
      {(items.length === 0)
        ? <EmptyCart message={"Your cart is empty."} />
        : <ActiveCart total={total} items={items} clear={clear} />}
    </div>
  );
});

export default connect((state) => {
  const preFormattedTotal = reduce(
      (sum, {id, quantity}) => sum + products[id].price * quantity,
      0,
      state.cart.items
    );

  return {
    items: state.cart.items,
    total: preFormattedTotal.toFixed(2),
  };
})(Cart);
